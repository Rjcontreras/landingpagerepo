import { CardItems } from './carditems'; 

export interface Tarjeta{
	contact_links_box_header_color : string, 
	contact_links_box_header_text : string,
	contact_links_box_header_text_color : string,
	card_item : carditem
}

export type carditem = CardItems[];