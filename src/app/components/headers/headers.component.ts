import { Component, OnInit ,Input} from '@angular/core';


@Component({
  selector: 'app-headers',
  templateUrl: './headers.component.html',
  styleUrls: ['./headers.component.css']
})
export class HeadersComponent implements OnInit {
	
  @Input() Header1: any;  
  @Input() MenuOpt: any;
  color : string;
  logo : string;
  
  constructor() {
	this.color = '';
	this.logo = '';
  }
  
  ngOnInit(): void {
	  
	  this.color = this.Header1.header_top_two_backround;
	  this.logo = this.Header1.header_logo1;
	  
  }

}
