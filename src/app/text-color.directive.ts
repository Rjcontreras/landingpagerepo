import { Directive , ElementRef , Input , OnInit} from '@angular/core';

@Directive({
  selector: '[appTextColor]'
})
export class TextColorDirective implements OnInit{

	constructor(private el: ElementRef) { }
  
	@Input('appTextColor') TextColor: string ;
	
	ngOnInit() {
		this.el.nativeElement.style.color  = this.TextColor;
	}

}
