export interface TarjetaCard {
card_icon : string,
card_title : string,
card_subtitle : string,
card_description : string,
card_link : string,
card_link_target : string
}