import { FooterContent } from './footer_content';

export interface Footer{
background_general : string,
top_line_grosor : string,
top_line_color : string,
background_corner : string,
background_content : string,
background_content_t : string,
first_bottom_image : string,
second_bottom_image : string,
footerF_bottom_image : string,
footerF_bottom_background : string,
footer_background_color : string,
red_twitter : string,
red_facebook : string,
red_instagram : string,
red_youtube : string,
footer_color_red_text : string,
footer_bottom_link : string,
footer_bottom_link_name : string,
footer_bottom_link_target : string,
footer_bottom_link_font_size : string,
footer_up_content : footerContent,
footer_bottom_content : footerContent
}

export type footerContent = FooterContent[];