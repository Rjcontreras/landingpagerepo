import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeadersComponent } from './components/headers/headers.component';
import { HeaderToneDirective } from './header-tone.directive';
import { TextColorDirective } from './text-color.directive';
import { HeaderUpperComponent } from './components/header-upper/header-upper.component';
import { NavComponent } from './components/nav/nav.component';


@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    HeaderToneDirective,
    TextColorDirective,
    HeaderUpperComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	CarouselModule,
	BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
