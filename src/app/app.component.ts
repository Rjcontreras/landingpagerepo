import { Component } from '@angular/core';
import { CustomPage } from './others/interfaces';
import { SubMenu } from './others/submenu';
import { Menu } from './others/menu';  
import { Tarjeta } from './others/tarjeta'; 
import { Footer } from './others/footer';
import { TarjetaCard } from './others/tarjetacard'; 
import { CarruselSlideO } from './others/carrusel_slideo'; 
import myJSON  from '../assets/dataEsquema.json';
import { OwlOptions } from 'ngx-owl-carousel-o';
declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Comprobar-Validez-App';
  customOptions: OwlOptions = {
	  loop:true,
			animateOut: 'fadeOut',
            animateIn: 'fadeIn',
			margin:0,
			nav:true,
			smartSpeed: 500,
			autoplay: true,
			autoplayTimeout:8000,
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			dots: false,
			navText: [ '<span class="govco-icon govco-icon-shortl-arrow size-1x"></span>', '<span class="govco-icon govco-icon-shortr-arrow size-1x"></span>' ],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				800:{
					items:1
				},
				1024:{
					items:1
				}
			}
  };
  public headerOne: CustomPage;
  public menu: Menu[];
  public card_tarjeta: Tarjeta;
  public footer : Footer;
  public tarjeta_card : TarjetaCard[];
  public image_carrusel :string;
  public carrusel_slideo : CarruselSlideO[];
  constructor() {
    this.headerOne = myJSON[0].header_one;
	
	this.menu = myJSON[0].menu_options;
	 
	this.card_tarjeta = myJSON[0].tarjeta;
	this.footer = myJSON[0].footer;
	this.tarjeta_card = myJSON[0].tarjeta_card;
	this.carrusel_slideo = myJSON[0].carrusel;
	this.image_carrusel =  myJSON[0].imagen_carrusel;
	
  }

   public isDraw(draw : string){
	  
	  if (draw == 'SI'){
			return true;
	  }else{
			return false;
	  }
  }	
  
  public isLink(link : string){
	  
	  if (link == 'NO'){
			return false;
	  }else{
			return true;
	  }
  }
}
