export interface CarruselSlideO{
carrusel1_title : string,
carrusel1_description : string,
carrusel1_boton_name : string,
carrusel1_boton_link : string,
carrusel1_boton_target : string,
carrusel1_boton_color : string,
carrusel1_boton_colortext : string
}