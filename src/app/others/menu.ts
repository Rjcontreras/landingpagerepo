import { SubMenu } from './submenu';

export interface Menu {
	menu_enable : string,
	menu_name : string,
	menu_link : string,
	menu_target : string,
	submenu_enable : string,
	submenu : sub_menu_t 
}



export type sub_menu_t = SubMenu[];