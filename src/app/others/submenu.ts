export interface SubMenu{
	submenu_name : string,
	submenu_target : string,
	submenu_link : string
}