import { Directive, ElementRef , Input , OnInit} from '@angular/core';

@Directive({
  selector: '[appHeaderTone]'
})
export class HeaderToneDirective implements OnInit{
	
 

  constructor(private el: ElementRef) {
        
	 
	
  }
  
   @Input('appHeaderTone') HeaderToneColor: string ;	
  
   
  
  
  ngOnInit() {
     this.el.nativeElement.style.backgroundColor  = this.HeaderToneColor;
  }
  
  
   
   

}
