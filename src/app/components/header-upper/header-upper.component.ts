import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'app-header-upper',
  templateUrl: './header-upper.component.html',
  styleUrls: ['./header-upper.component.css']
})
export class HeaderUpperComponent implements OnInit {
	
	colorM : string;
	
	constructor() {
		this.colorM = '';	  
	}
  
  
  
  @Input() HeaderUp: any;
  @Input() Menu: any;  

  ngOnInit(): void {
	  this.colorM = this.HeaderUp.menu_background;
  }

}
