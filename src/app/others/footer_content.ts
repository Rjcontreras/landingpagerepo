import { FooterList } from './footer_list'; 

export interface FooterContent {
footer_draw : string,
footer_background : string,
footer_title : string,
footer_color_text : string,
footer_title_font_size : string,
footer_padding : string,
footer_list : footerList
}

export type footerList = FooterList[];