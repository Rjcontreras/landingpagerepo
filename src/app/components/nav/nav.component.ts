import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }
  @Input() OptMenu: any; 

  ngOnInit(): void {
	  
  }
  
  public isvalid(enamenu : string){
	  
	  if (enamenu == 'SI'){
			return true;
	  }else{
			return false;
	  }
  }

}
